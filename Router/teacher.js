const mysql = require('../database')
const express = require("express")
const teacher = express.Router()

function compareTime(cur, time, diffTime) {
    let curtime = cur.substring(8, 10)
    let timeOpen = time.substring(0, 2)
    let ct = parseInt(curtime)
    let to = parseInt(timeOpen)

    for (let i = 0; i < diffTime + 1; i++) {
        if (ct === to) {
            return true
        }
        to++
    }

    return false
}
function generate_token(length) {
    var a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".split("");
    var b = [];
    for (var i = 0; i < length; i++) {
        var j = (Math.random() * (a.length - 1)).toFixed(0);
        b[i] = a[j];
    }
    return b.join("");
}

function dataIsEmpty(value) {
    return value === undefined || value === '' || value === null || value.length === 0
}

class Subject {
    constructor(courseGroupId, courseCode, courseName, studyFormat, group) {
        this.courseGroupId = courseGroupId
        this.courseCode = courseCode
        this.courseName = courseName
        this.studyFormat = studyFormat
        this.group = group
    }
}

class Student {
    constructor(studentId, studentCode, studentName) {
        this.studentId = studentId
        this.studentCode = studentCode
        this.studentName = studentName
    }
}

function convertTime(time) {
    let time1 = time.substring(0, 1)
    let time2 = time.substring(1, 2)
    let time3 = time.substring(0, 2)
    let time4 = time.substring(5, 7)
    let t = time.substring(2, 7)
    let reTime = 12

    if (time4 === 'PM') {
        for (let i = 0; i < 12; i++) {
            reTime = reTime + 1
            if (time1 === "0") {
                if (time2 === i + 1 + "") {
                    return reTime + t
                }
            }
            else {
                if (time3 === i + 1 + "") {
                    return reTime + t
                }
            }
        }
    }
    return time
}

teacher.post('/add', (req, res) => {

    let sumTeacher = req.body.length
    let nameId = 571
    for (let i = 0; i < sumTeacher; i++) {
        let addName = false
        let name_en = req.body[i].name_en
        let name_th = req.body[i].name_th
        let code = req.body[i].code
        let id = req.body[i].id

        // if (!addName) {

        //     let insert_nameen = "INSERT INTO `name_en`(`prefix`, `firstName`, `middleName`, `lastName`) VALUES (?,?,?,?)"
        //     let insert_nameth = "INSERT INTO `name_th`(`prefix`, `firstName`, `middleName`, `lastName`) VALUES (?,?,?,?)"

        //     mysql.query(insert_nameen, [name_en.prefix, name_en.firstName, name_en.middleName, name_en.lastName], function (err, result) {
        //         if (err) {
        //             res.send(err)
        //         }
        //     })

        //     mysql.query(insert_nameth, [name_th.prefix, name_th.firstName, name_th.middleName, name_th.lastName], function (err, result) {
        //         if (err) {
        //             res.send(err)
        //         }
        //     })
        //     addName = true
        //     if (i + 1 === sumTeacher) {
        //         res.status(201).send({ message: 'Insert student successfully' })
        //     }
        // }

        let addTeacherSql = "INSERT INTO `teacher` (`teacher_id`, `teacher_code`, `name_en`, `name_th`) VALUES (? , ? , ? , ?)"
        mysql.query(addTeacherSql, [id, code, nameId, nameId], function (err, result) {
            if (err) {
                res.send({ error: err })
            }
            if (i + 1 === sumTeacher) {
                res.status(201).send({ message: 'Insert teacher successfully' })
            }
        })
        nameId += 1
    }

})

teacher.post('/login', (req, res) => {
    let receiveUsername = req.body.username
    let receivePassword = req.body.password

    let sql = "SELECT * FROM `customer_account`"
    mysql.query(sql, function (err, result, fields) {
        let loginSuccess = false
        let worngUsernameandPassword = false
        let Insufficientparameter = false
        let cusId
        let token = generate_token(32)

        if (err) {
            res.send(err)
        }

        if (dataIsEmpty(receiveUsername) && dataIsEmpty(receivePassword)) {
            Insufficientparameter = true
            res.json({ statusCode: 2, message: "Insufficient parameter: username or password is required parameter" })
        }
        else if (dataIsEmpty(receiveUsername)) {
            Insufficientparameter = true
            res.json({ statusCode: 3, message: "Insufficient parameter: username is required parameter" })
        }
        else if (dataIsEmpty(receivePassword)) {
            Insufficientparameter = true
            res.json({ statusCode: 4, message: "Insufficient parameter: password is required parameter" })
        }

        if (!Insufficientparameter) {
            let sql = "UPDATE `customer_account` SET token = ? WHERE id = ?"
            for (let i = 0; i < result.length; i++) {
                if (receiveUsername === result[i].username && receivePassword === result[i].password) {
                    loginSuccess = true
                    cusId = result[i].id
                    if (dataIsEmpty(result[i].token)) {
                        mysql.query(sql, [token, cusId], function (err, result) {
                            if (err) {
                                res.send(err)
                            }
                        })
                    }
                }
                else if (receiveUsername != result[i].username) {
                    if (receivePassword != result[i].password) {
                        worngUsernameandPassword = true
                    }
                }
            }
            let getToken = "SELECT token FROM customer_account WHERE id = ?"

            mysql.query(getToken, cusId, function (err, result) {
                if (loginSuccess) {
                    res.send({ statusCode: 0, message: "Login Sucess", token: result[0].token })
                }
                else if (worngUsernameandPassword) {
                    res.send({ statusCode: 1, message: "Username or password incorrect" })
                }
            })

        }
    })
})

teacher.get('/courses', (req, res) => {
    let token = req.header('token')
    let getToken = "SELECT * FROM `customer_account` WHERE role = 'T'"

    mysql.query(getToken, (err, result) => {
        if (err) {
            res.send(err)
        }
        let teacherId
        let courses = []
        let s
        for (let i = 0; i < result.length; i++) {
            if (result[i].token === token) {
                teacherId = result[i].teacher_id
            }
        }
        let day
        let dayOfWeek = "SELECT DAYOFWEEK(CURRENT_DATE)-1 as day"
        mysql.query(dayOfWeek, (err, result) => {
            if (err) {
                res.send(err)
            }
            if (day === 0) {
                day = 7
            }
            day = result[0].day
            let curtime = "SELECT CURRENT_TIMESTAMP()+1 as time"
            mysql.query(curtime, (err, time) => {
                let tc = "20191120135560"
                let timeCur = time[0].time + ''
                let lecture = "SELECT * FROM `course_group` INNER JOIN courses ON course_group.course_id = courses.id INNER JOIN lecture ON lecture.course_group=course_group.id WHERE `teacher_id`= ?"
                mysql.query(lecture, teacherId, (err, lecture) => {
                    let courseGroupIds = []
                    if (err) throw err
                    for (let j = 0; j < lecture.length; j++) {
                        if (lecture[j].day === day) {
                            let timeOpen = parseInt(lecture[j].timeOpen)
                            let timeClose = parseInt(lecture[j].timeClosed)
                            let diffTime = timeClose - timeOpen
                            let t = compareTime(timeCur, convertTime(lecture[j].timeOpen), diffTime)
                            let courseGroupId = lecture[j].course_group
                            let courseCode = lecture[j].course_code
                            let courseName = lecture[j].course_name
                            let studyFormat = {
                                format: "lecture",
                                nowStudy: t,
                                day: lecture[j].day,
                                timeOpen: convertTime(lecture[j].timeOpen),
                                timeClose: convertTime(lecture[j].timeClosed),
                                room: lecture[j].room
                            }
                            let group = lecture[j].group_name
                            s = new Subject(courseGroupId, courseCode, courseName, studyFormat, lecture[j].group_name)
                            courseGroupIds.push(lecture[j].course_group)
                            courses.push(s)
                        }
                    }
                    let lab = "SELECT * FROM `course_group` INNER JOIN courses ON course_group.course_id = courses.id INNER JOIN lab ON lab.course_group=course_group.id WHERE `teacher_id`= ?"
                    mysql.query(lab, teacherId, (err, lab) => {
                        let courseGroupIds = []
                        if (err) throw err
                        for (let j = 0; j < lab.length; j++) {
                            if (lab[j].day === day) {
                                let timeOpen = parseInt(lab[j].timeOpen)
                                let timeClose = parseInt(lab[j].timeClosed)
                                let diffTime = timeClose - timeOpen
                                let t = compareTime(timeCur, convertTime(lab[j].timeOpen), diffTime)
                                let courseGroupId = lab[j].course_group
                                let courseCode = lab[j].course_code
                                let courseId = lab[j].course_id
                                let courseName = lab[j].course_name
                                let studyFormat = {
                                    format: "lab",
                                    nowStudy: t,
                                    day: lab[j].day,
                                    timeOpen: convertTime(lab[j].timeOpen),
                                    timeClose: convertTime(lab[j].timeClosed),
                                    room: lab[j].room
                                }
                                let group = lab[j].group_name
                                s = new Subject(courseGroupId, courseCode, courseName, studyFormat, lab[j].group_name)
                                courseGroupIds.push(lab[j].course_group)
                                courses.push(s)
                            }
                        }
                        if (!dataIsEmpty(courses)) {
                            res.send(courses)
                        }
                    })
                    if (dataIsEmpty(courses)) {
                        let lab = "SELECT * FROM `course_group` INNER JOIN courses ON course_group.course_id = courses.id INNER JOIN lab ON lab.course_group=course_group.id WHERE `teacher_id`= ?"
                        mysql.query(lab, teacherId, (err, lab) => {
                            let courseGroupIds = []
                            if (err) throw err
                            for (let j = 0; j < lab.length; j++) {
                                if (lab[j].day === day) {
                                    let timeOpen = parseInt(lab[j].timeOpen)
                                    let timeClose = parseInt(lab[j].timeClosed)
                                    let diffTime = timeClose - timeOpen
                                    let t = compareTime(timeCur, convertTime(lab[j].timeOpen), diffTime)
                                    let courseGroupId = lab[j].course_group
                                    let courseCode = lab[j].course_code
                                    let courseId = lab[j].course_id
                                    let courseName = lab[j].course_name
                                    let studyFormat = {
                                        format: "lab",
                                        nowStudy: t,
                                        day: lab[j].day,
                                        timeOpen: convertTime(lab[j].timeOpen),
                                        timeClose: convertTime(lab[j].timeClosed),
                                        room: lab[j].room
                                    }
                                    let group = lab[j].group_name
                                    s = new Subject(courseGroupId, courseCode, courseName, studyFormat, lab[j].group_name)
                                    courseGroupIds.push(lab[j].course_group)
                                    courses.push(s)
                                }
                            }
                            res.send(courses)
                        })
                    }
                })
            })
        })
    })
})

teacher.get('/liststudent/:id', (req, res) => {
    let courseGroupId = req.params.id
    let token = req.header('token')
    let get_token = "SELECT * FROM `customer_account`"
    let isNotTecher = true
    let Insufficientparameter = false

    if (dataIsEmpty(courseGroupId)) {
        Insufficientparameter = true
        res.send({ statusCode: 2, message: 'Insufficient parameter: courseGroupId' })
    }

    if (!Insufficientparameter) {
        mysql.query(get_token, (err, result) => {
            if (err) throw err
            for (let i = 0; i < result.length; i++) {
                if (result[i].token === token) {
                    isNotTecher = false
                    let studentList = []
                    let getStudentlists = "SELECT student.`student_code`,name_th.prefix,name_th.firstName,name_th.lastName FROM `student` INNER JOIN course_detail ON student.student_code=course_detail.student_code INNER JOIN name_th ON name_th.nameth_id=student.name_th WHERE course_detail.course_group = ?"
                    mysql.query(getStudentlists, courseGroupId, (err, result) => {
                        if (err) throw err
                        for (let i = 0; i < result.length; i++) {
                            let studentId = result[i].id
                            let studentCode = result[i].student_code
                            let studentName = result[i].prefix + " " + result[i].firstName + " " + result[i].lastName
                            let s = new Student(studentId, studentCode, studentName)
                            studentList.push(s)
                        }
                        res.json(studentList)
                    })
                }
            }
            if (isNotTecher) {
                res.send({ statusCode: 1, message: 'is not teacher' })
            }
        })
    }
})

teacher.get('/all/courses', (req, res) => {
    let token = req.header('token')
    let getToken = "SELECT * FROM `customer_account` WHERE role = 'T'"
    let isNotTecher = true
    mysql.query(getToken, (err, result) => {
        if (err) {
            res.send(err)
        }
        let courses = []
        for (let i = 0; i < result.length; i++) {
            if (result[i].token === token) {
                let getLec = false
                isNotTecher = false
                let teacher = result[i]
                let findTeacherId = "SELECT * FROM `course_group` INNER JOIN courses ON course_group.course_id = courses.id INNER JOIN lab ON lab.course_group=course_group.id WHERE `teacher_id`= ?"
                mysql.query(findTeacherId, teacher.teacher_id, (err, result) => {
                    if (err) throw err
                    for (let i = 0; i < result.length; i++) {
                        if (result[i].status === null) {
                            courseGroupId = result[i].course_group
                            courseCode = result[i].course_code
                            courseName = result[i].course_name
                            studyFormat = {
                                format: "lab",
                                day: result[i].day,
                                timeOpen: convertTime(result[i].timeOpen),
                                timeClose: convertTime(result[i].timeClosed),
                                room: result[i].room
                            }
                            group = result[i].group_name
                            let s = new Subject(courseGroupId, courseCode, courseName, studyFormat, group)
                            courses.push(s)
                        }

                    }
                    getLec = true
                    if (getLec) {
                        let lab = "SELECT * FROM `course_group` INNER JOIN courses ON course_group.course_id = courses.id INNER JOIN lecture ON lecture.course_group=course_group.id WHERE `teacher_id`= ?"
                        mysql.query(lab, teacher.teacher_id, (err, rlec) => {
                            if (err) throw err
                            for (let i = 0; i < rlec.length; i++) {
                                if (rlec[i].status === null) {
                                    courseGroupId = rlec[i].course_group
                                    courseCode = rlec[i].course_code
                                    courseName = rlec[i].course_name
                                    studyFormat = {
                                        format: "lecture",
                                        day: rlec[i].day,
                                        timeOpen: convertTime(rlec[i].timeOpen),
                                        timeClose: convertTime(rlec[i].timeClosed),
                                        room: rlec[i].room
                                    }
                                    group = rlec[i].group_name
                                    let s = new Subject(courseGroupId, courseCode, courseName, studyFormat, group)
                                    courses.push(s)
                                }
                            }
                            res.json(courses)
                        })
                    }
                })
            }
        }
        if (isNotTecher) {
            res.send({ statusCode: 1, message: 'is not teacher' })
        }
    })
})

teacher.post('/add/course/special', (req, res) => {
    let courseGroupId = req.body.courseGroupId
    let format = req.body.format
    let day = req.body.day
    let timeOpen = req.body.timeOpen
    let timeClose = req.body.timeClose
    let room = req.body.room
    let status = "W"
    let Insufficientparameter = false
    let isNotTecher = true
    let token = req.header('token')
    let getToken = "SELECT * FROM `customer_account` WHERE role = 'T'"

    if (dataIsEmpty(courseGroupId) || dataIsEmpty(format) || dataIsEmpty(day) || dataIsEmpty(timeOpen) || dataIsEmpty(timeClose) || dataIsEmpty(room)) {
        Insufficientparameter = true
        res.send({ statusCode: 2, message: 'Insufficient parameter' })
    }

    if (!Insufficientparameter) {
        mysql.query(getToken, (err, result) => {
            if (err) {
                res.send(err)
            }
            for (let i = 0; i < result.length; i++) {
                if (result[i].token === token) {
                    if (format === 'lab') {
                        let insertCourse = "INSERT INTO `lab`(`day`, `timeOpen`, `timeClosed`, `room`, `course_group`, `status`) VALUES (?,?,?,?,?,?)"
                        mysql.query(insertCourse, [day, timeOpen, timeClose, room, courseGroupId, status], (err, result) => {
                            if (err) {
                                res.send(err)
                            }
                            res.send({ statusCode: 0, message: 'OK' })
                        })
                    } else if (format === 'lecture') {
                        let insertCourse = "INSERT INTO `lecture`(`day`, `timeOpen`, `timeClosed`, `room`, `course_group`, `status`) VALUES (?,?,?,?,?,?)"
                        mysql.query(insertCourse, [day, timeOpen, timeClose, room, courseGroupId, status], (err, result) => {
                            if (err) {
                                res.send(err)
                            }
                            res.send({ statusCode: 0, message: 'OK' })
                        })
                    }
                    isNotTecher = false
                }
            }
            if (isNotTecher) {
                res.send({ statusCode: 1, message: 'is not teacher' })
            }
        })
    }
})

teacher.get('/course/special', (req, res) => {
    let token = req.header('token')
    let getToken = "SELECT * FROM `customer_account` WHERE role = 'T'"
    let isNotTecher = true
    mysql.query(getToken, (err, result) => {
        if (err) {
            res.send(err)
        }
        let courses = []
        for (let i = 0; i < result.length; i++) {
            if (result[i].token === token) {
                isNotTecher = false
                let getLec = false
                let teacher = result[0]
                let findTeacherId = "SELECT * FROM `course_group` INNER JOIN courses ON course_group.course_id = courses.id INNER JOIN lab ON lab.course_group=course_group.id WHERE `teacher_id`= ?"
                mysql.query(findTeacherId, teacher.teacher_id, (err, result) => {
                    if (err) throw err
                    for (let i = 0; i < result.length; i++) {
                        if (result[i].status === 'W') {
                            courseGroupId = result[i].course_group
                            courseCode = result[i].course_code
                            courseName = result[i].course_name
                            studyFormat = {
                                format: "lab",
                                day: result[i].day,
                                timeOpen: convertTime(result[i].timeOpen),
                                timeClose: convertTime(result[i].timeClosed),
                                room: result[i].room
                            }
                            group = result[i].group_name
                            let s = new Subject(courseGroupId, courseCode, courseName, studyFormat, group)
                            courses.push(s)
                        }

                    }
                    getLec = true
                    if (getLec) {
                        let lab = "SELECT * FROM `course_group` INNER JOIN courses ON course_group.course_id = courses.id INNER JOIN lecture ON lecture.course_group=course_group.id WHERE `teacher_id`= ?"
                        mysql.query(lab, teacher.teacher_id, (err, rlec) => {
                            if (err) throw err
                            for (let i = 0; i < rlec.length; i++) {
                                if (rlec[i].status === 'W') {
                                    courseGroupId = rlec[i].course_group
                                    courseCode = rlec[i].course_code
                                    courseName = rlec[i].course_name
                                    studyFormat = {
                                        format: "lecture",
                                        day: rlec[i].day,
                                        timeOpen: convertTime(rlec[i].timeOpen),
                                        timeClose: convertTime(rlec[i].timeClosed),
                                        room: rlec[i].room
                                    }
                                    group = rlec[i].group_name
                                    let s = new Subject(courseGroupId, courseCode, courseName, studyFormat, group)
                                    courses.push(s)
                                }
                            }
                            res.json(courses)
                        })
                    }
                })
            }
        }
        if (isNotTecher) {
            res.send({ statusCode: 1, message: 'is not teacher' })
        }
    })
})


teacher.get('/datecheck/:id', (req, res) => {
    let courseGroupId = req.params.id
    let token = req.header('token')
    let getToken = "SELECT * FROM `customer_account` WHERE role = 'T'"
    let isNotTecher = true
    let Insufficientparameter = false

    if (dataIsEmpty(courseGroupId)) {
        Insufficientparameter = true
        res.send({ statusCode: 2, message: 'Insufficient parameter' })
    }

    if (!Insufficientparameter) {
        mysql.query(getToken, (err, result) => {
            if (err) {
                res.send(err)
            }
            let courseTime = { courseId: '', courseName: '', courseGroup: '', courseGroupId: '', dateCheck: '' }
            for (let i = 0; i < result.length; i++) {
                if (result[i].token === token) {
                    isNotTecher = false
                    let getDate = "SELECT courses.course_id,courses.course_name,course_group.group_name,course_group.id,check_date.date FROM `course_group` JOIN courses ON course_group.course_id=courses.id JOIN check_date ON course_group.id = check_date.course_group WHERE course_group.id = ?"
                    mysql.query(getDate, courseGroupId, (err, result) => {
                        if (err) {
                            res.send(err)
                        }
                        if (dataIsEmpty(result)) {
                            res.send({ statusCode: -1, message: 'not found datecheck' })
                        }
                        else {
                            courseTime.courseId = result[0].course_id
                            courseTime.courseName = result[0].course_name
                            courseTime.courseGroup = result[0].group_name
                            courseTime.courseGroupId = result[0].id
                            let time = []
                            for (let i = 0; i < result.length; i++) {
                                let dateCheck = new Date(result[i].date).toLocaleDateString()
                                time.push(dateCheck)
                            }
                            courseTime.dateCheck = time
                            res.send(courseTime)
                        }
                    })
                }
            }
            if (isNotTecher) {
                res.send({ statusCode: 1, message: 'is not teacher' })
            }
        })
    }
})

teacher.post('/score/check/', (req, res) => {
    let dateCheck = req.body.dateCheck
    let courseGroupId = req.body.courseGroupId
    let token = req.header('token')
    let getToken = "SELECT * FROM `customer_account` WHERE role = 'T'"
    let Insufficientparameter = false
    let isNotTecher = true

    console.log(dateCheck)

    if (dataIsEmpty(dateCheck)) {
        Insufficientparameter = true
        res.send({ statusCode: 2, message: 'Insufficient parameter' })
    }

    if (!Insufficientparameter) {
        mysql.query(getToken, (err, result) => {
            if (err) {
                res.send(err)
            }
            for (let i = 0; i < result.length; i++) {
                if (result[i].token === token) {
                    isNotTecher = false
                    let timeChecks = []
                    let getcourseCheck = "SELECT * FROM `course_check` INNER JOIN student ON student.student_code=course_check.student_id JOIN name_th ON name_th.nameth_id = student.name_th WHERE course_group = ?"
                    mysql.query(getcourseCheck, courseGroupId, (err, courseCheck) => {
                        for (let i = 0; i < courseCheck.length; i++) {
                            let timeCheck = new Date(courseCheck[i].time_check).toLocaleDateString()
                            if (timeCheck === dateCheck) {
                                let t = courseCheck[i]
                                delete t.student_code
                                delete t.course_group
                                delete t.name_en
                                delete t.name_th
                                delete t.nameth_id
                                delete t.prefix
                                delete t.middleName
                                timeChecks.push(t)
                            }
                        }
                        res.send(timeChecks)
                    })
                }
            }
            if (isNotTecher) {
                res.send({ statusCode: 1, message: 'is not teacher' })
            }
        })
    }

})
teacher.put('/edit/score/:id', (req, res) => {
    let scoreCheckId = req.params.id
    let token = req.header('token')
    let getToken = "SELECT * FROM `customer_account` WHERE role = 'T'"
    let Insufficientparameter = false
    let isNotTecher = true

    if (dataIsEmpty(scoreCheckId)) {
        Insufficientparameter = true
        res.send({ statusCode: 2, message: 'Insufficient parameter' })
    }

    if (!Insufficientparameter) {
        mysql.query(getToken, (err, result) => {
            if (err) {
                res.send(err)
            }
            for (let i = 0; i < result.length; i++) {
                if (result[i].token === token) {
                    isNotTecher = false
                    let updateScore = "UPDATE `course_check` SET `score` = '1',`status` = 'E' WHERE `course_check`.`id` = ?"
                    mysql.query(updateScore, scoreCheckId, (err, result) => {
                        if (err) {
                            res.send(err)
                        }
                        res.send({ statusCode: 0, message: 'OK' })
                    })
                }
            }
            if (isNotTecher) {
                res.send({ statusCode: 1, message: 'is not teacher' })
            }
        })
    }
})

module.exports = teacher