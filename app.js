const express = require('express')
const app = express()
const cors = require('cors')

const teacherRouter = require('./Router/teacher')
const studentRouter = require('./Router/student')
const machineRouter = require('./Router/machine')
const subjectRouter = require('./Router/subject')
const adminRouter = require('./Router/admin')

const bodyParser = require('body-parser');
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true, parameterLimit: 50000 }));

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*")
    res.header("Access-Control-Allow-Headers", "*")
    res.header("Access-Control-Allow-Methods", "*")
    next();
});

app.use(express.json())
app.use(cors())

app.get('/', (req, res) => {
    res.send('Hello world!!!!')
})

app.use('/teacher', teacherRouter)
app.use('/student', studentRouter)
app.use('/machine', machineRouter)
app.use('/subject', subjectRouter)
app.use('/admin', adminRouter)

module.exports = app