const mysql = require('../database')
const express = require("express")
const admin = express.Router()

function generate_token(length) {
    var a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".split("");
    var b = [];
    for (var i = 0; i < length; i++) {
        var j = (Math.random() * (a.length - 1)).toFixed(0);
        b[i] = a[j];
    }
    return b.join("");
}


function dataIsEmpty(value) {
    return value === undefined || value === '' || value === null || value.length === 0
}

admin.post('/login', (req, res) => {
    let receiveUsername = req.body.username
    let receivePassword = req.body.password

    let sql = "SELECT * FROM `customer_account`"
    mysql.query(sql, function (err, result, fields) {
        let loginSuccess = false
        let worngUsernameandPassword = false
        let Insufficientparameter = false
        let cusId
        let token = generate_token(32)

        if (err) {
            res.send(err)
        }

        if (dataIsEmpty(receiveUsername) && dataIsEmpty(receivePassword)) {
            Insufficientparameter = true
            res.json({ statusCode: 2, message: "Insufficient parameter: username or password is required parameter" })
        }
        else if (dataIsEmpty(receiveUsername)) {
            Insufficientparameter = true
            res.json({ statusCode: 3, message: "Insufficient parameter: username is required parameter" })
        }
        else if (dataIsEmpty(receivePassword)) {
            Insufficientparameter = true
            res.json({ statusCode: 4, message: "Insufficient parameter: password is required parameter" })
        }

        if (!Insufficientparameter) {
            let sql = "UPDATE `customer_account` SET token = ? WHERE id = ?"
            for (let i = 0; i < result.length; i++) {
                if (receiveUsername === result[i].username && receivePassword === result[i].password) {
                    loginSuccess = true
                    cusId = result[i].id
                    if (dataIsEmpty(result[i].token)) {
                        mysql.query(sql, [token, cusId], function (err, result) {
                            if (err) {
                                res.send(err)
                            }
                        })
                    }
                }
                else if (receiveUsername != result[i].username) {
                    if (receivePassword != result[i].password) {
                        worngUsernameandPassword = true
                    }
                }
            }
            let getToken = "SELECT customer_account.role,customer_account.token,name_th.firstName,name_th.lastName FROM customer_account JOIN teacher ON teacher.teacher_id = customer_account.teacher_id JOIN name_th ON name_th.nameth_id=teacher.name_th WHERE id = ?"

            mysql.query(getToken, cusId, function (err, result) {
                if (err) {
                    res.send(err)
                }
                else if (dataIsEmpty(result)) {
                    let getAdmin = "SELECT * FROM `customer_account` WHERE role = 'S'"
                    mysql.query(getAdmin, (err, getadmin) => {
                        res.send({ statusCode: 0, message: "Login Sucess", token: getadmin[0].token, role: getadmin[0].role })
                    })
                }
                else if (loginSuccess) {
                    res.send({ statusCode: 0, message: "Login Sucess", token: result[0].token, role: result[0].role, firstName: result[0].firstName, lastName: result[0].lastName })
                }
                else if (worngUsernameandPassword) {
                    res.send({ statusCode: 1, message: "Username or password incorrect" })
                }
            })

        }
    })
})

admin.post('/add/teacher', (req, res) => {
    let token = req.header('token')
    let username = req.body.username
    let password = req.body.password
    let teacherId = req.body.teacherId
    let role = 'T'
    let isNotStaff = true
    let Insufficientparameter = false
    let tokenTeacher = generate_token(32)

    if (dataIsEmpty(username) || dataIsEmpty(password) || dataIsEmpty(teacherId)) {
        Insufficientparameter = true
        res.send({ statusCode: 2, message: 'Insufficient parameter' })
    }

    if (!Insufficientparameter) {
        let get_token = "SELECT * FROM `customer_account` WHERE role = 'S'"
        mysql.query(get_token, (err, result) => {
            if (err) {
                res.send(err)
            }
            for (let i = 0; i < result.length; i++) {
                if (result[i].token === token) {
                    isNotStaff = false
                    let insertTeacher = "INSERT INTO `customer_account`(`username`, `password`, `role`, `teacher_id`,`token`) VALUES (?,?,?,?,?)"
                    mysql.query(insertTeacher, [username, password, role, teacherId, tokenTeacher], (err, result) => {
                        if (err) throw err
                        res.send({ statusCode: 0, message: 'OK' })
                    })
                }
            }
            if (isNotStaff) {
                res.send({ statusCode: 1, message: 'is not staff' })
            }
        })
    }

})

admin.post('/search/techer', (req, res) => {
    let teachers = []
    let teacherName = "%" + req.body.teacherName + "%"
    let token = req.header('token')
    let isNotStaff = true
    let Insufficientparameter = false

    if (dataIsEmpty(teacherName)) {
        Insufficientparameter = true
        res.send({ statusCode: 2, message: 'Insufficient parameter' })
    }
    if (!Insufficientparameter) {
        let get_token = "SELECT * FROM `customer_account` WHERE role = 'S'"
        mysql.query(get_token, (err, result) => {
            if (err) {
                res.send(err)
            }
            for (let i = 0; i < result.length; i++) {
                if (result[i].token === token) {
                    isNotStaff = false
                    let searchTeacher = "SELECT * FROM `teacher` JOIN name_th ON teacher.name_th=name_th.nameth_id WHERE name_th.firstName LIKE ? LIMIT 10"
                    mysql.query(searchTeacher, teacherName, (err, result) => {
                        if (err) {
                            res.send(err)
                        }
                        if (dataIsEmpty(result[0])) {
                            res.send({ statusCode: -1, message: 'teacher not found' })
                        }
                        else {
                            for (let i = 0; i < result.length; i++) {
                                let teacher = result[i]
                                delete teacher.name_th
                                delete teacher.name_en
                                delete teacher.nameth_id
                                teachers.push(teacher)
                            }
                            res.send({ statusCode: 0, teacher: teachers })
                        }
                    })
                }
            }
            if (isNotStaff) {
                res.send({ statusCode: 1, message: 'is not admin' })
            }
        })
    }

})

admin.post('/search/student', (req, res) => {
    let studentId = req.body.studentId
    let token = req.header('token')
    let isNotStaff = true
    let Insufficientparameter = false

    if (dataIsEmpty(studentId)) {
        Insufficientparameter = true
        res.send({ statusCode: 2, message: 'Insufficient parameter' })
    }

    if (!Insufficientparameter) {
        let get_token = "SELECT * FROM `customer_account` WHERE role = 'S'"
        mysql.query(get_token, (err, result) => {
            if (err) {
                res.send(err)
            }
            for (let i = 0; i < result.length; i++) {
                if (result[i].token === token) {
                    isNotStaff = false
                    let searchStudent = "SELECT * FROM `student` JOIN name_th ON student.name_th=name_th.nameth_id WHERE student.student_code = ?"
                    mysql.query(searchStudent, studentId, (err, result) => {
                        if (err) {
                            res.send(err)
                        }
                        if (dataIsEmpty(result[0])) {
                            res.send({ statusCode: -1, message: 'student not found' })
                        }
                        else {
                            let student = result[0]
                            delete student.name_th
                            delete student.name_en
                            delete student.nameth_id
                            delete student.prefix
                            delete student.middleName
                            res.send({ statusCode: 0, student: student })
                        }
                    })
                }
            }
            if (isNotStaff) {
                res.send({ statusCode: 1, message: 'is not admin' })
            }
        })
    }
})

let waitPerson
admin.get('/person/:id', (req, res) => {
    let token = req.header('token')
    let isNotStaff = false
    let get_token = "SELECT * FROM `customer_account` WHERE role = 'S'"

    let Insufficientparameter = false

    if (dataIsEmpty(req.params.id)) {
        Insufficientparameter = true
        res.send({ statusCode: 2, message: 'Insufficient parameter' })
    }

    if (!Insufficientparameter) {
        mysql.query(get_token, (err, result) => {
            if (err) {
                res.send(err)
            }
            for (let i = 0; i < result.length; i++) {
                if (result[i].token === token) {
                    waitPerson = req.params.id
                    res.send({ statusCode: 0 })
                }
                else {
                    isNotStaff = true
                }
            }
            if (isNotStaff) {
                res.send({ statusCode: 1, message: 'is not admin' })
            }
        })
    }
})

admin.post('/add/fingerprint/', (req, res) => {
    let fingerprint = req.body.fingerprint
    let verify = req.body.verify
    let role = req.body.role
    let Insufficientparameter = false
    let token = req.header('token')
    let isNotStaff = false

    if (dataIsEmpty(fingerprint) || dataIsEmpty(verify) || dataIsEmpty(role)) {
        Insufficientparameter = true
        res.send({ statusCode: 3, message: 'Insufficient parameter' })
    }
    else if (dataIsEmpty(waitPerson)) {
        res.send({ statusCode: 2, message: 'no studentId' })
    }
    else if (!Insufficientparameter) {
        let get_token = "SELECT * FROM `customer_account` WHERE role = 'S'"
        mysql.query(get_token, (err, result) => {
            if (err) {
                res.send(err)
            }
            for (let i = 0; i < result.length; i++) {
                if (result[i].token === token) {
                    isNotStaff = false
                    let insertFingerprint = "INSERT INTO `fingerprint`(`fingerprint`, `verify`, `role`, `student_id`) VALUES (?,?,?,?)"
                    mysql.query(insertFingerprint, [fingerprint, verify, role, waitPerson], (err, result) => {
                        if (err) {
                            res.send(err)
                        }
                        res.send({ statusCode: 0, message: 'insert OK', studentId: waitPerson })
                    })
                }
            }
            if (isNotStaff) {
                res.send({ statusCode: 1, message: 'is not admin' })
            }
        })
    }
})

admin.post('/add/machine/owner', (req, res) => {
    let machineName = req.body.machineName
    let teacherId = req.body.teacherId
    let Insufficientparameter = false
    let token = req.header('token')
    let isNotStaff = true
    let get_token = "SELECT * FROM `customer_account` WHERE role = 'S'"

    if (dataIsEmpty(machineName) || dataIsEmpty(teacherId)) {
        Insufficientparameter = true
        res.send({ statusCode: 2, message: 'Insufficient parameter' })
    }
    else if (!Insufficientparameter) {
        mysql.query(get_token, (err, result) => {
            if (err) {
                res.send(err)
            }
            for (let i = 0; i < result.length; i++) {
                if (result[i].token === token) {
                    isNotStaff = false
                    let updateOwner = "UPDATE `machines` SET `teacher_id`=? WHERE machine_name = ?"
                    mysql.query(updateOwner, [teacherId, machineName], (err, result) => {
                        if (err) {
                            res.send(err)
                        }
                        let searchTeacher = "SELECT name_th.firstName,name_th.lastName,machines.machine_name,machines.status,machines.teacher_id FROM `teacher` JOIN name_th ON teacher.name_th=name_th.nameth_id JOIN machines ON machines.teacher_id= teacher.teacher_id WHERE teacher.teacher_id = ?"
                        mysql.query(searchTeacher, teacherId, (err, result) => {
                            if (err) {
                                res.send(err)
                            }
                            let name = result[0].firstName + " " + result[0].lastName
                            res.send({ statusCode: 0, machine_name: result[0].machine_name, status: result[0].status, teacherId: result[0].teacher_id, name: name })
                        })
                    })
                }
            }
            if (isNotStaff) {
                res.send({ statusCode: 1, message: 'is not admin' })
            }
        })
    }

})

admin.get('/machine/list', (req, res) => {
    let token = req.header('token')
    let isNotStaff = true
    let get_token = "SELECT * FROM `customer_account` WHERE role = 'S'"
    mysql.query(get_token, (err, result) => {
        if (err) {
            res.send(err)
        }
        let machines = []
        let machine = { machine_name: "", status: "", teacherId: "", name: "",ip:"" }
        let count = 0
        for (let i = 0; i < result.length; i++) {
            if (result[i].token === token) {
                isNotStaff = false
                let get_machine = "SELECT * FROM `machines`"
                mysql.query(get_machine, (err, r) => {
                    if (err) {
                        res.send(err)
                    }
                    if (dataIsEmpty(r)) {
                        res.send({ status: -1, message: 'not found machines' })
                    }
                    for (let i = 0; i < r.length; i++) {
                        let machine = r[i]
                        if (dataIsEmpty(machine.teacher_id)) {
                            machine.machine_name = machine.machine_name
                            machine.status = machine.status
                            machine.name = 'admin'
			    machine.ip = machine.ip_address
                            machines.push(machine)
                            count++
                        }
                        else {
                            let getTecher = "SELECT name_th.firstName,name_th.lastName,machines.machine_name,machines.status,machines.teacher_id FROM `teacher` JOIN name_th ON teacher.name_th=name_th.nameth_id JOIN machines ON machines.teacher_id= teacher.teacher_id WHERE teacher.teacher_id = ?"
                            mysql.query(getTecher, machine.teacher_id, (err, result) => {
                                if (err) {
                                    res.send(err)
                                }
                                else {
                                    let name = result[0].firstName + " " + result[0].lastName
                                    machine.machine_name = machine.machine_name
                                    machine.status = machine.status
                                    machine.name = name
                                    machine.teacher_id = machine.teacher_id
				    machine.ip = machine.ip_address
                                    machines.push(machine)
                                    count++
                                }
                                if (count === r.length) {
                                    res.send(machines)
                                }
                            })
                        }
                        if (count === r.length) {
                            res.send(machines)
                        }
                    }
                })
            }
        }
        if (isNotStaff) {
            res.send({ statusCode: 1, message: 'is not admin' })
        }
    })
})

admin.get('/student/:id', (req, res) => {
    let n = req.params.id
    let Insufficientparameter = false
    let token = req.header('token')
    let isNotStaff = false
    let get_token = "SELECT * FROM `customer_account` WHERE role = 'S'"

    if (dataIsEmpty(n)) {
        Insufficientparameter = true
        res.send({ statusCode: 2, message: 'Insufficient parameter' })
    }
    else if (!Insufficientparameter) {
        mysql.query(get_token, (err, result) => {
            if (err) {
                res.send(err)
            }
            for (let i = 0; i < result.length; i++) {
                if (result[i].token === token) {
                    isNotStaff = false
                    let getStudent = "SELECT student.student_code,name_th.firstName,name_th.lastName FROM `student` JOIN name_th ON student.name_th=name_th.nameth_id ORDER BY `student`.`student_code` ASC LIMIT ?,10"
                    mysql.query(getStudent, (n - 1) * 10, (err, result) => {
                        if (err) {
                            res.send(err)
                        }
                        res.send(result)
                    })
                }
            }
            if (isNotStaff) {
                res.send({ statusCode: 1, message: 'is not admin' })
            }
        })
    }
})

admin.get('/teacher/:id', (req, res) => {
    let n = req.params.id
    let Insufficientparameter = false
    let token = req.header('token')
    let isNotStaff = false
    let get_token = "SELECT * FROM `customer_account` WHERE role = 'S'"

    if (dataIsEmpty(n)) {
        Insufficientparameter = true
        res.send({ statusCode: 2, message: 'Insufficient parameter' })
    }
    else if (!Insufficientparameter) {
        mysql.query(get_token, (err, result) => {
            if (err) {
                res.send(err)
            }
            for (let i = 0; i < result.length; i++) {
                if (result[i].token === token) {
                    let getTecher = "SELECT teacher.teacher_id,name_th.firstName,name_th.lastName FROM `teacher` JOIN name_th ON teacher.name_th=name_th.nameth_id ORDER BY teacher.teacher_id ASC LIMIT ?,10"
                    mysql.query(getTecher, (n - 1) * 10, (err, result) => {
                        if (err) {
                            res.send(err)
                        }
                        res.send(result)
                    })
                }
                else {
                    isNotStaff = true
                }
            }
            if (isNotStaff) {
                res.send({ statusCode: 1, message: 'is not admin' })
            }
        })
    }
})

admin.post('/turnoff/machine', (req, res) => {
    let machineName = req.body.machineName
    let Insufficientparameter = false
    let token = req.header('token')
    let isNotStaff = false
    let get_token = "SELECT * FROM `customer_account` WHERE role = 'S'"

    if (dataIsEmpty(machineName)) {
        Insufficientparameter = true
        res.send({ statusCode: 2, message: 'Insufficient parameter' })
    }
    else if (!Insufficientparameter) {
        mysql.query(get_token, (err, result) => {
            if (err) {
                res.send(err)
            }
            for (let i = 0; i < result.length; i++) {
                if (result[i].token === token) {
                    let turnoffMachine = "UPDATE `machines` SET `status`='D' WHERE machine_name = ?"
                    mysql.query(turnoffMachine, machineName, (err, result) => {
                        let getMachine = "SELECT * FROM `machines` WHERE machine_name = ?"
                        mysql.query(getMachine, machineName, (err, machine) => {
                            let m = machine[0]
                            delete m.teacher_id
                            res.send(m)
                        })
                    })
                }
                else {
                    isNotStaff = true
                }
            }
            if (isNotStaff) {
                res.send({ statusCode: 1, message: 'is not admin' })
            }
        })
    }
})

admin.post('/turnon/machine', (req, res) => {
    let machineName = req.body.machineName
    let Insufficientparameter = false
    let token = req.header('token')
    let isNotStaff = false
    let get_token = "SELECT * FROM `customer_account` WHERE role = 'S'"

    if (dataIsEmpty(machineName)) {
        Insufficientparameter = true
        res.send({ statusCode: 2, message: 'Insufficient parameter' })
    }
    else if (!Insufficientparameter) {
        mysql.query(get_token, (err, result) => {
            if (err) {
                res.send(err)
            }
            for (let i = 0; i < result.length; i++) {
                if (result[i].token === token) {
                    let turnoffMachine = "UPDATE `machines` SET `status`='N' WHERE machine_name = ?"
                    mysql.query(turnoffMachine, machineName, (err, result) => {
                        let getMachine = "SELECT * FROM `machines` WHERE machine_name = ?"
                        mysql.query(getMachine, machineName, (err, machine) => {
                            let m = machine[0]
                            delete m.teacher_id
                            res.send(m)
                        })
                    })
                }
                else {
                    isNotStaff = true
                }
            }
            if (isNotStaff) {
                res.send({ statusCode: 1, message: 'is not admin' })
            }
        })
    }
})

admin.post('/machine/change', (req, res) => {
    let machineName = req.body.machineName
    let Insufficientparameter = false
    let token = req.header('token')
    let isNotStaff = false
    let get_token = "SELECT * FROM `customer_account` WHERE role = 'S'"

    if (dataIsEmpty(machineName)) {
        Insufficientparameter = true
        res.send({ statusCode: 2, message: 'Insufficient parameter' })
    }
    else if (!Insufficientparameter) {
        mysql.query(get_token, (err, result) => {
            if (err) {
                res.send(err)
            }
            for (let i = 0; i < result.length; i++) {
                if (result[i].token === token) {
                    let becomeAdmin = "UPDATE `machines` SET `teacher_id`= null WHERE machine_name = ?"
                    mysql.query(becomeAdmin, machineName, (err, result) => {
                        if (err) {
                            res.send(err)
                        }
                        res.send({ statusCode: 0 })
                    })
                }
                else {
                    isNotStaff = true
                }
            }
            if (isNotStaff) {
                res.send({ statusCode: 1, message: 'is not admin' })
            }
        })
    }
})
module.exports = admin