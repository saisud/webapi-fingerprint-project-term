const mysql = require('../database')
const express = require("express")
const subject = express.Router()

class Subject {
    constructor(courseGroupId, courseId, courseCode, courseName, studyFormat, group) {
        this.courseGroupId = courseGroupId
        this.courseCode = courseCode
        this.courseId = courseId
        this.courseName = courseName
        this.studyFormat = studyFormat
        this.group = group
    }
}

class Student {
    constructor(studentId, studentCode, studentName) {
        this.studentId = studentId
        this.studentCode = studentCode
        this.studentName = studentName
    }
}

function makeCredit(credit) {
    let remakeCredit = ''
    for (let i = 0; i < credit.length; i++) {
        if (i === credit.length - 1) {
            remakeCredit += credit[i]
        }
        else {
            remakeCredit += credit[i] + '-'
        }
    }
    return '(' + remakeCredit + ')'
}

function convertTime(time) {
    let time1 = time.substring(0, 1)
    let time2 = time.substring(1, 2)
    let time3 = time.substring(0, 2)
    let time4 = time.substring(5, 7)
    let t = time.substring(2, 7)
    let reTime = 12

    if (time4 === 'PM') {
        for (let i = 0; i < 12; i++) {
            reTime = reTime + 1
            if (time1 === "0") {
                if (time2 === i + 1 + "") {
                    return reTime + t
                }
            }
            else {
                if (time3 === i + 1 + "") {
                    return reTime + t
                }
            }
        }
    }
    return time
}

function compareTime(cur, time) {
    let curtime = cur.substring(8, 10)
    let timeOpen = time.substring(0, 2)
    if (curtime === timeOpen) {
        return true
    }
    return false
}

function dataIsEmpty(value) {
    return value === undefined || value === '' || value === null || value.length === 0
}

subject.post('/add', (req, res) => {
    let subject = req.body.subjects
    for (let i = 0; i < subject.length; i++) {
        let courseId = subject[i].id
        // let courseCode = subject[i].code
        // let courseCredit = subject[i].credit
        // let credit = [subject[i].creditTime.lab, subject[i].creditTime.lab, subject[i].creditTime.selfStudy]
        // let c = makeCredit(credit)
        // let courseName = subject[i].name
        // let year = subject[i].year
        // let semester = subject[i].semester
        // let courseAdd = "INSERT INTO `courses`(`course_name`, `course_id`, `course_code`, `credit`, `credit_time`, `year`, `semester`) VALUES (?,?,?,?,?,?,?)"
        // mysql.query(courseAdd, [courseName, courseId, courseCode, courseCredit, c, year, semester], (err, result) => {
        //     if (err) {
        //         res.send(err)
        //     }
        //     else if (i === subject.length - 1) {
        //         res.send({ message: 'insert OK' })
        //     }
        // })


        let groups = subject[i].groups
        for (let j = 0; j < groups.length; j++) {
            let searchCourseId = "SELECT * FROM `courses` WHERE course_id = ?"
            mysql.query(searchCourseId, courseId, (err, result) => {
                if (err) {
                    res.send(err)
                }
                let courseId = result[0].id
                let serchCourseGroupId = "SELECT id FROM `course_group` WHERE course_id = ? && group_name = ? "
                mysql.query(serchCourseGroupId, [courseId, groups[j].group], (err, result) => {
                    if (err) {
                        res.send(err)
                    }
                    let lecHasTimeStudy = groups[j].lecture.hasTimeStudy
                    if (lecHasTimeStudy === true) {
                        let day = groups[j].lecture.day
                        let timeOpen = groups[j].lecture.timeOpen
                        let timeClosed = groups[j].lecture.timeClosed
                        let room = groups[j].lecture.room
                        let courseGroupId = result[0].id
                        let insertLecture = "INSERT INTO `lecture`(`day`, `timeOpen`, `timeClosed`, `room`, `course_group`) VALUES (?,?,?,?,?)"
                        mysql.query(insertLecture, [day, timeOpen, timeClosed, room, courseGroupId], (err, result) => {
                            if (err) {
                                res.send(err)
                            }
                        })
                    }
                    let labHasTimeStudy = groups[j].lab.hasTimeStudy
                    if (labHasTimeStudy === true) {
                        let day = groups[j].lab.day
                        let timeOpen = groups[j].lab.timeOpen
                        let timeClosed = groups[j].lab.timeClosed
                        let room = groups[j].lab.room
                        let courseGroupId = result[0].id
                        let insertLecture = "INSERT INTO `lab`(`day`, `timeOpen`, `timeClosed`, `room`, `course_group`) VALUES (?,?,?,?,?)"
                        mysql.query(insertLecture, [day, timeOpen, timeClosed, room, courseGroupId], (err, result) => {
                            if (err) {
                                res.send(err)
                            }
                        })
                    }
                })
            })
        }
    }
})

subject.post('/course/group', (req, res) => {
    let course = req.body.course
    for (let i = 0; i < course.length; i++) {
        let courseId = course[i].courseId
        let group = course[i].group

        let searchCourseId = "SELECT * FROM `courses` WHERE course_id = ?"
        mysql.query(searchCourseId, courseId, (err, result) => {
            if (err) {
                res.send(err)
            }
            let ncourseId = result[0].id
            let teacherId = req.body.teacher.id
            let editTeacher = "UPDATE `course_group` SET `teacher_id` = ? WHERE group_name = ? && course_id = ?"
            mysql.query(editTeacher, [teacherId, group, ncourseId], (err, result) => {
                if (err) {
                    res.send(err)
                }
            })
        })

    }
})

subject.post('/course/student', (req, res) => {
    let courseId = req.body.courseId
    let group = req.body.groups[0].group
    let students = req.body.groups[0].students
    for (let i = 0; i < students.length; i++) {
        let addStudent = "INSERT INTO `course_detail`(`course_group`, `student_code`) VALUES (?,?)"
        mysql.query(addStudent, [258, students[i]], (err, result) => {
            if (err) {
                res.send(err)
            }
        })
    }
})

subject.post('/test', (req, res) => {
    let token = req.header('token')
    let getToken = "SELECT * FROM `customer_account` WHERE role = 'T'"

    mysql.query(getToken, (err, result) => {
        if (err) {
            res.send(err)
        }
        let teacherId
        let courses = []
        let s
        for (let i = 0; i < result.length; i++) {
            if (result[i].token === token) {
                teacherId = result[i].teacher_id
            }
        }
        let day
        let dayOfWeek = "SELECT DAYOFWEEK('2019-11-19')-1 as day"
        mysql.query(dayOfWeek, (err, result) => {
            if (err) {
                res.send(err)
            }
            if (day === 0) {
                day = 7
            }
            day = result[0].day
            let curtime = "SELECT CURRENT_TIMESTAMP()+1 as time"
            mysql.query(curtime, (err, time) => {
                let tc = "20191120135560"
                let timeCur = time[0].time + ''
                let lecture = "SELECT * FROM `course_group` INNER JOIN courses ON course_group.course_id = courses.id INNER JOIN lecture ON lecture.course_group=course_group.id WHERE `teacher_id`= ?"
                mysql.query(lecture, teacherId, (err, lecture) => {
                    let courseGroupIds = []
                    if (err) throw err
                    for (let j = 0; j < lecture.length; j++) {
                        if (lecture[j].day === day) {
                            let timeOpen = parseInt(lecture[j].timeOpen)
                            let timeClose = parseInt(lecture[j].timeClosed)
                            let diffTime = timeClose - timeOpen
                            let t = compareTime(tc, convertTime(lecture[j].timeOpen), diffTime)
                            if (t) {
                                let courseGroupId = lecture[j].course_group
                                let courseCode = lecture[j].course_code
                                let courseId = lecture[j].course_id
                                let courseName = lecture[j].course_name
                                let studyFormat = {
                                    format: "lecture",
                                    day: lecture[j].day,
                                    timeOpen: convertTime(lecture[j].timeOpen),
                                    timeClose: convertTime(lecture[j].timeClosed),
                                    room: lecture[j].room
                                }
                                let group = lecture[j].group_name
                                s = new Subject(courseGroupId, courseId, courseCode, courseName, studyFormat, lecture[j].group_name)
                                courseGroupIds.push(lecture[j].course_group)
                                courses.push(s)
                            }
                        }
                    }
                    let lab = "SELECT * FROM `course_group` INNER JOIN courses ON course_group.course_id = courses.id INNER JOIN lab ON lab.course_group=course_group.id WHERE `teacher_id`= ?"
                    mysql.query(lab, teacherId, (err, lab) => {
                        let courseGroupIds = []
                        if (err) throw err
                        for (let j = 0; j < lab.length; j++) {
                            if (lab[j].day === day) {
                                let timeOpen = parseInt(lab[j].timeOpen)
                                let timeClose = parseInt(lab[j].timeClosed)
                                let diffTime = timeClose - timeOpen
                                let t = compareTime(tc, convertTime(lab[j].timeOpen), diffTime)
                                if (t) {
                                    let courseGroupId = lab[j].course_group
                                    let courseCode = lab[j].course_code
                                    let courseId = lab[j].course_id
                                    let courseName = lab[j].course_name
                                    let studyFormat = {
                                        format: "lab",
                                        day: lab[j].day,
                                        timeOpen: convertTime(lab[j].timeOpen),
                                        timeClose: convertTime(lab[j].timeClosed),
                                        room: lab[j].room
                                    }
                                    let group = lab[j].group_name
                                    s = new Subject(courseGroupId, courseId, courseCode, courseName, studyFormat, lab[j].group_name)
                                    courseGroupIds.push(lab[j].course_group)
                                    courses.push(s)
                                }
                            }
                        }
                        if (!dataIsEmpty(courses)) {
                            res.send(courses)
                        }
                    })
                    if (dataIsEmpty(courses)) {
                        let lab = "SELECT * FROM `course_group` INNER JOIN courses ON course_group.course_id = courses.id INNER JOIN lab ON lab.course_group=course_group.id WHERE `teacher_id`= ?"
                        mysql.query(lab, teacherId, (err, lab) => {
                            let courseGroupIds = []
                            if (err) throw err
                            for (let j = 0; j < lab.length; j++) {
                                if (lab[j].day === day) {
                                    let timeOpen = parseInt(lab[j].timeOpen)
                                    let timeClose = parseInt(lab[j].timeClosed)
                                    let diffTime = timeClose - timeOpen
                                    let t = compareTime(tc, convertTime(lab[j].timeOpen), diffTime)
                                    if (t) {
                                        let courseGroupId = lab[j].course_group
                                        let courseCode = lab[j].course_code
                                        let courseId = lab[j].course_id
                                        let courseName = lab[j].course_name
                                        let studyFormat = {
                                            format: "lab",
                                            day: lab[j].day,
                                            timeOpen: convertTime(lab[j].timeOpen),
                                            timeClose: convertTime(lab[j].timeClosed),
                                            room: lab[j].room
                                        }
                                        let group = lab[j].group_name
                                        s = new Subject(courseGroupId, courseId, courseCode, courseName, studyFormat, lab[j].group_name)
                                        courseGroupIds.push(lab[j].course_group)
                                        courses.push(s)
                                    }
                                }
                            }
                            res.send(courses)
                        })
                    }
                })
            })
        })
    })
})
module.exports = subject