-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 25, 2019 at 05:47 PM
-- Server version: 5.7.28-0ubuntu0.18.04.4
-- PHP Version: 7.2.24-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `it59160761`
--

-- --------------------------------------------------------

--
-- Table structure for table `building`
--

CREATE TABLE `building` (
  `id` int(11) NOT NULL,
  `building_id` varchar(196) NOT NULL,
  `building_name` varchar(196) NOT NULL,
  `campusId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `check_date`
--

CREATE TABLE `check_date` (
  `id` int(11) NOT NULL,
  `course_group` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `course_name` varchar(200) NOT NULL,
  `course_id` varchar(196) NOT NULL,
  `course_code` varchar(196) NOT NULL,
  `credit` int(11) NOT NULL,
  `credit_time` varchar(64) NOT NULL,
  `year` varchar(4) NOT NULL,
  `semester` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `course_check`
--

CREATE TABLE `course_check` (
  `id` int(11) NOT NULL,
  `course_group` int(11) NOT NULL,
  `student_id` varchar(64) NOT NULL,
  `score` int(11) NOT NULL DEFAULT '0',
  `time_check` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `time_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('N','E') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `course_detail`
--

CREATE TABLE `course_detail` (
  `id` int(11) NOT NULL,
  `course_group` int(11) NOT NULL,
  `student_code` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `course_group`
--

CREATE TABLE `course_group` (
  `id` int(11) NOT NULL,
  `group_name` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `teacher_id` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_account`
--

CREATE TABLE `customer_account` (
  `id` int(11) NOT NULL,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `role` enum('S','T') NOT NULL,
  `token` varchar(32) DEFAULT NULL,
  `teacher_id` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `emp_id` int(11) NOT NULL,
  `firstname` varchar(128) NOT NULL,
  `lastname` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `phone_number` varchar(9) NOT NULL,
  `phone_region` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `employee_account`
--

CREATE TABLE `employee_account` (
  `id` int(11) NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(196) NOT NULL,
  `position` enum('N','S','O') NOT NULL,
  `employee_id` int(11) NOT NULL,
  `token` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `fingerprint`
--

CREATE TABLE `fingerprint` (
  `id` int(11) NOT NULL,
  `fingerprint` longtext NOT NULL,
  `verify` varchar(64) NOT NULL,
  `role` enum('T','S') NOT NULL,
  `student_id` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `lab`
--

CREATE TABLE `lab` (
  `id` int(11) NOT NULL,
  `day` int(11) NOT NULL,
  `timeOpen` varchar(64) NOT NULL,
  `timeClosed` varchar(64) NOT NULL,
  `room` varchar(64) NOT NULL,
  `course_group` int(11) NOT NULL,
  `status` enum('W','S') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `lecture`
--

CREATE TABLE `lecture` (
  `id` int(11) NOT NULL,
  `day` int(11) NOT NULL,
  `timeOpen` varchar(64) NOT NULL,
  `timeClosed` varchar(64) NOT NULL,
  `room` varchar(64) NOT NULL,
  `course_group` int(11) NOT NULL,
  `status` enum('W','S') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `machines`
--

CREATE TABLE `machines` (
  `machine_name` varchar(64) NOT NULL,
  `status` enum('N','D') NOT NULL,
  `teacher_id` varchar(64) DEFAULT NULL,
  `ip_address` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `name_en`
--

CREATE TABLE `name_en` (
  `nameen_id` int(11) NOT NULL,
  `firstName` varchar(196) NOT NULL,
  `prefix` varchar(196) DEFAULT NULL,
  `middleName` varchar(196) DEFAULT NULL,
  `lastName` varchar(196) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `name_th`
--

CREATE TABLE `name_th` (
  `nameth_id` int(11) NOT NULL,
  `firstName` varchar(196) NOT NULL,
  `prefix` varchar(196) DEFAULT NULL,
  `middleName` varchar(196) DEFAULT NULL,
  `lastName` varchar(196) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `register_custormer`
--

CREATE TABLE `register_custormer` (
  `customer_id` int(11) NOT NULL,
  `customer_company_name` varchar(250) NOT NULL,
  `customer_tel` varchar(9) NOT NULL,
  `region` varchar(2) NOT NULL,
  `customer_email` varchar(90) NOT NULL,
  `status` enum('A','P','R') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `student_code` varchar(64) NOT NULL,
  `name_en` int(11) NOT NULL,
  `name_th` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `teacher`
--

CREATE TABLE `teacher` (
  `teacher_id` varchar(64) NOT NULL,
  `teacher_code` varchar(64) NOT NULL,
  `name_th` int(11) NOT NULL,
  `name_en` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_account`
--

CREATE TABLE `user_account` (
  `user_id` int(11) NOT NULL,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `token` varchar(196) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `building`
--
ALTER TABLE `building`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `check_date`
--
ALTER TABLE `check_date`
  ADD PRIMARY KEY (`id`),
  ADD KEY `course_group` (`course_group`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course_check`
--
ALTER TABLE `course_check`
  ADD PRIMARY KEY (`id`),
  ADD KEY `course_group` (`course_group`),
  ADD KEY `student_id` (`student_id`);

--
-- Indexes for table `course_detail`
--
ALTER TABLE `course_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `course_group` (`course_group`),
  ADD KEY `student_id` (`student_code`);

--
-- Indexes for table `course_group`
--
ALTER TABLE `course_group`
  ADD PRIMARY KEY (`id`),
  ADD KEY `course_id` (`course_id`),
  ADD KEY `teacher_id` (`teacher_id`);

--
-- Indexes for table `customer_account`
--
ALTER TABLE `customer_account`
  ADD PRIMARY KEY (`id`),
  ADD KEY `teacher_id` (`teacher_id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`emp_id`);

--
-- Indexes for table `employee_account`
--
ALTER TABLE `employee_account`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `employee_id` (`employee_id`);

--
-- Indexes for table `fingerprint`
--
ALTER TABLE `fingerprint`
  ADD PRIMARY KEY (`id`),
  ADD KEY `person_id` (`student_id`);

--
-- Indexes for table `lab`
--
ALTER TABLE `lab`
  ADD PRIMARY KEY (`id`),
  ADD KEY `course_group` (`course_group`);

--
-- Indexes for table `lecture`
--
ALTER TABLE `lecture`
  ADD PRIMARY KEY (`id`),
  ADD KEY `course_group` (`course_group`);

--
-- Indexes for table `machines`
--
ALTER TABLE `machines`
  ADD PRIMARY KEY (`machine_name`),
  ADD KEY `teacher_id` (`teacher_id`);

--
-- Indexes for table `name_en`
--
ALTER TABLE `name_en`
  ADD PRIMARY KEY (`nameen_id`);

--
-- Indexes for table `name_th`
--
ALTER TABLE `name_th`
  ADD PRIMARY KEY (`nameth_id`);

--
-- Indexes for table `register_custormer`
--
ALTER TABLE `register_custormer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`student_code`),
  ADD KEY `name_en` (`name_en`),
  ADD KEY `name_th` (`name_th`);

--
-- Indexes for table `teacher`
--
ALTER TABLE `teacher`
  ADD PRIMARY KEY (`teacher_id`),
  ADD KEY `name_th` (`name_th`),
  ADD KEY `name_en` (`name_en`);

--
-- Indexes for table `user_account`
--
ALTER TABLE `user_account`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `building`
--
ALTER TABLE `building`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;
--
-- AUTO_INCREMENT for table `check_date`
--
ALTER TABLE `check_date`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;
--
-- AUTO_INCREMENT for table `course_check`
--
ALTER TABLE `course_check`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `course_detail`
--
ALTER TABLE `course_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=310;
--
-- AUTO_INCREMENT for table `course_group`
--
ALTER TABLE `course_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=259;
--
-- AUTO_INCREMENT for table `customer_account`
--
ALTER TABLE `customer_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `emp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `employee_account`
--
ALTER TABLE `employee_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `fingerprint`
--
ALTER TABLE `fingerprint`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `lab`
--
ALTER TABLE `lab`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `lecture`
--
ALTER TABLE `lecture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `name_en`
--
ALTER TABLE `name_en`
  MODIFY `nameen_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7425;
--
-- AUTO_INCREMENT for table `name_th`
--
ALTER TABLE `name_th`
  MODIFY `nameth_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7425;
--
-- AUTO_INCREMENT for table `register_custormer`
--
ALTER TABLE `register_custormer`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user_account`
--
ALTER TABLE `user_account`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `check_date`
--
ALTER TABLE `check_date`
  ADD CONSTRAINT `check_date_ibfk_1` FOREIGN KEY (`course_group`) REFERENCES `course_group` (`id`);

--
-- Constraints for table `course_check`
--
ALTER TABLE `course_check`
  ADD CONSTRAINT `course_check_ibfk_1` FOREIGN KEY (`course_group`) REFERENCES `course_group` (`id`),
  ADD CONSTRAINT `course_check_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_code`);

--
-- Constraints for table `course_detail`
--
ALTER TABLE `course_detail`
  ADD CONSTRAINT `course_detail_ibfk_1` FOREIGN KEY (`course_group`) REFERENCES `course_group` (`id`),
  ADD CONSTRAINT `course_detail_ibfk_2` FOREIGN KEY (`student_code`) REFERENCES `student` (`student_code`);

--
-- Constraints for table `course_group`
--
ALTER TABLE `course_group`
  ADD CONSTRAINT `course_group_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`),
  ADD CONSTRAINT `course_group_ibfk_2` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`teacher_id`);

--
-- Constraints for table `customer_account`
--
ALTER TABLE `customer_account`
  ADD CONSTRAINT `customer_account_ibfk_1` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`teacher_id`);

--
-- Constraints for table `employee_account`
--
ALTER TABLE `employee_account`
  ADD CONSTRAINT `employee_account_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`emp_id`);

--
-- Constraints for table `fingerprint`
--
ALTER TABLE `fingerprint`
  ADD CONSTRAINT `fingerprint_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_code`);

--
-- Constraints for table `lab`
--
ALTER TABLE `lab`
  ADD CONSTRAINT `lab_ibfk_1` FOREIGN KEY (`course_group`) REFERENCES `course_group` (`id`);

--
-- Constraints for table `lecture`
--
ALTER TABLE `lecture`
  ADD CONSTRAINT `lecture_ibfk_1` FOREIGN KEY (`course_group`) REFERENCES `course_group` (`id`);

--
-- Constraints for table `machines`
--
ALTER TABLE `machines`
  ADD CONSTRAINT `machines_ibfk_1` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`teacher_id`);

--
-- Constraints for table `student`
--
ALTER TABLE `student`
  ADD CONSTRAINT `student_ibfk_1` FOREIGN KEY (`name_en`) REFERENCES `name_en` (`nameen_id`),
  ADD CONSTRAINT `student_ibfk_2` FOREIGN KEY (`name_th`) REFERENCES `name_th` (`nameth_id`);

--
-- Constraints for table `teacher`
--
ALTER TABLE `teacher`
  ADD CONSTRAINT `teacher_ibfk_1` FOREIGN KEY (`name_en`) REFERENCES `name_en` (`nameen_id`),
  ADD CONSTRAINT `teacher_ibfk_2` FOREIGN KEY (`name_th`) REFERENCES `name_th` (`nameth_id`);

--
-- Constraints for table `user_account`
--
ALTER TABLE `user_account`
  ADD CONSTRAINT `user_account_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `register_custormer` (`customer_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
