const mysql = require('../database')
const express = require("express")
const machine = express.Router()
const apiKey = "Gin589vuS2hNDZfJl97pAgimYK2VAns3"

function dataIsEmpty(value) {
    return value === undefined || value === '' || value === null || value.length === 0
}

function compareTime(cur, time, diffTime) {
    let curtime = cur.substring(8, 10)
    let timeOpen = time.substring(0, 2)
    let ct = parseInt(curtime)
    let to = parseInt(timeOpen)

    for (let i = 0; i < diffTime + 1; i++) {
        if (ct === to) {
            return true
        }
        to++
    }

    return false
}

function convertTime(time) {
    let time1 = time.substring(0, 1)
    let time2 = time.substring(1, 2)
    let time3 = time.substring(0, 2)
    let time4 = time.substring(5, 7)
    let t = time.substring(2, 7)
    let reTime = 12

    if (time4 === 'PM') {
        for (let i = 0; i < 12; i++) {
            reTime = reTime + 1
            if (time1 === "0") {
                if (time2 === i + 1 + "") {
                    return reTime + t
                }
            }
            else {
                if (time3 === i + 1 + "") {
                    return reTime + t
                }
            }
        }
    }
    return time
}


class Subject {
    constructor(courseGroupId, courseId, courseCode, courseName, studyFormat, group, student) {
        this.courseGroupId = courseGroupId
        this.courseId = courseId
        this.courseCode = courseCode
        this.courseName = courseName
        this.studyFormat = studyFormat
        this.group_name = group
        this.student = student
    }
}

class Student {
    constructor(studentCode, studentName) {
        this.studentCode = studentCode
        this.studentName = studentName
    }
}
machine.post('/add', (req, res) => {
    let rapiKey = req.header('apiKey')
    let machineName = req.body.machineName
    let status = 'N'
    let ip = req.body.ip
    let notMachine = true

    if (apiKey === rapiKey) {
        let getMachine = "SELECT * FROM `machines` WHERE machine_name = ?"
        mysql.query(getMachine, machineName, (err, result) => {
            if (err) throw err
            for (let i = 0; i < result.length; i++) {
                if (machineName === result[i].machine_name) {
                    if (err) {
                        res.send(err)
                    }
                    let machine = result[i]
                    let getToken = "SELECT token FROM `teacher` JOIN customer_account ON teacher.teacher_id = customer_account.teacher_id WHERE customer_account.teacher_id = ?"
                    mysql.query(getToken, machine.teacher_id, (err, result) => {
                        if (dataIsEmpty(result)) {
                            if (dataIsEmpty(ip)) {
                                let update_ip = "UPDATE `machines` SET `ip_address` = ? WHERE machine_name = ?"
                                mysql.query(update_ip, ['', machineName], (err, result) => {
                                    if (err) {
                                        res.send(err)
                                    }
                                })
                                let getToken = "SELECT token FROM `customer_account` WHERE role = 'S'"
                                mysql.query(getToken, (err, result) => {
                                    res.send({ machineName: machine.machine_name, status: machine.status, token: result[0].token, role: 'S' })
                                })
                            }
                            else {
                                let update_ip = "UPDATE `machines` SET `ip_address` = ? WHERE machine_name = ?"
                                mysql.query(update_ip, [ip, machineName], (err, result) => {
                                    if (err) {
                                        res.send(err)
                                    }
                                })
                                let getToken = "SELECT token FROM `customer_account` WHERE role = 'S'"
                                mysql.query(getToken, (err, result) => {
                                    res.send({ machineName: machine.machine_name, status: machine.status, token: result[0].token, role: 'S' })
                                })
                            }
                        }
                        else {
                            if (dataIsEmpty(ip)) {
                                let update_ip = "UPDATE `machines` SET `ip_address` = ? WHERE machine_name = ?"
                                mysql.query(update_ip, ['', machineName], (err, result) => {
                                    if (err) {
                                        res.send(err)
                                    }
                                })
                            }
                            else {
                                let update_ip = "UPDATE `machines` SET `ip_address` = ? WHERE machine_name = ?"
                                mysql.query(update_ip, [ip, machineName], (err, result) => {
                                    if (err) {
                                        res.send(err)
                                    }
                                })
                            }
                            res.send({ machineName: machine.machine_name, status: machine.status, token: result[0].token, role: 'T' })
                            let dayOfWeek = "SELECT DAYOFWEEK(CURRENT_TIMESTAMP)-1 as day"
                            mysql.query(dayOfWeek, (err, result) => {
                                if (err) {
                                    res.send(err)
                                }
                                let day = result[0].day
                                if (day === 0) {
                                    day = 7
                                }
                                let student = []
                                let findTeacherId = "SELECT course_group.id,lab.day,lab.timeOpen,lab.timeClosed FROM `course_group` INNER JOIN courses ON course_group.course_id = courses.id INNER JOIN lab ON lab.course_group=course_group.id WHERE `teacher_id`= ?"
                                mysql.query(findTeacherId, machine.teacher_id, (err, lab) => {
                                    if (err) {
                                        res.send(err)
                                    }
                                    for (let i = 0; i < lab.length; i++) {
                                        let courseDay = lab[i].day
                                        if (day === courseDay) {
                                            if (err) {
                                                res.send(err)
                                            }
                                            let curtime = "SELECT CURRENT_TIMESTAMP()+1 as time"
                                            mysql.query(curtime, (err, time) => {
                                                let timeCur = time[0].time + ''
                                                let timeOpen = parseInt(lab[i].timeOpen)
                                                let timeClose = parseInt(lab[i].timeClosed)
                                                let diffTime = timeClose - timeOpen
                                                let t = compareTime(timeCur, convertTime(lab[i].timeOpen), diffTime)
                                                if (t) {
                                                    let getStudent = "SELECT * FROM `course_detail` WHERE course_group = ?"
                                                    mysql.query(getStudent, lab[i].id, (err, student) => {
                                                        if (err) {
                                                            res.send(err)
                                                        }
                                                        let checkAlready = "SELECT * FROM `course_check` WHERE course_group = ?"
                                                        mysql.query(checkAlready, lab[i].id, (err, check) => {
                                                            if (err) {
                                                                res.send(err)
                                                            }
                                                            if (dataIsEmpty(check)) {
                                                                for (let j = 0; j < student.length; j++) {
                                                                    let setUpScore = "INSERT INTO `course_check`(`course_group`, `student_id`,`status`) VALUES (?,?,?)"
                                                                    mysql.query(setUpScore, [lab[i].id, student[j].student_code, status], (err, result) => {
                                                                        if (err) {
                                                                            res.send(err)
                                                                        }
                                                                    })
                                                                }
                                                                let setUpdate = "INSERT INTO `check_date`(`course_group`) VALUES (?)"
                                                                mysql.query(setUpdate, lab[i].id, (err, result) => {
                                                                    if (err) {
                                                                        res.send(err)
                                                                    }
                                                                })
                                                            }
                                                            else {
                                                                let dateCheck = new Date().toLocaleDateString()
                                                                let timeCheck = new Date(check[0].time_check).toLocaleDateString()
                                                                if (dateCheck === timeCheck) {

                                                                }
                                                                else {
                                                                    for (let j = 0; j < student.length; j++) {
                                                                        let setUpScore = "INSERT INTO `course_check`(`course_group`, `student_id`,`status`) VALUES (?,?,?)"
                                                                        mysql.query(setUpScore, [lab[i].id, student[j].student_code, status], (err, result) => {
                                                                            if (err) {
                                                                                res.send(err)
                                                                            }
                                                                        })
                                                                    }
                                                                    let setUpdate = "INSERT INTO `check_date`(`course_group`) VALUES (?)"
                                                                    mysql.query(setUpdate, lab[i].id, (err, result) => {
                                                                        if (err) {
                                                                            res.send(err)
                                                                        }
                                                                    })
                                                                }
                                                            }
                                                        })
                                                    })
                                                }
                                            })
                                        }
                                    }
                                })
                                let lecture = "SELECT course_group.id,lecture.day,lecture.timeOpen,lecture.timeClosed FROM `course_group` INNER JOIN courses ON course_group.course_id = courses.id INNER JOIN lecture ON lecture.course_group=course_group.id WHERE `teacher_id`= ?"
                                mysql.query(lecture, machine.teacher_id, (err, lecture) => {
                                    for (let i = 0; i < lecture.length; i++) {
                                        let courseDay = lecture[i].day
                                        if (day === courseDay) {
                                            let curtime = "SELECT CURRENT_TIMESTAMP()+1 as time"
                                            mysql.query(curtime, (err, time) => {
                                                let timeCur = time[0].time + ''
                                                let timeOpen = parseInt(lecture[i].timeOpen)
                                                let timeClose = parseInt(lecture[i].timeClosed)
                                                let diffTime = timeClose - timeOpen
                                                let t = compareTime(timeCur, convertTime(lecture[i].timeOpen), diffTime)
                                                if (t) {
                                                    let getStudent = "SELECT * FROM `course_detail` WHERE course_group = ?"
                                                    mysql.query(getStudent, lecture[i].id, (err, student) => {
                                                        if (err) {
                                                            res.send(err)
                                                        }
                                                        let checkAlready = "SELECT * FROM `course_check` WHERE course_group = ?"
                                                        mysql.query(checkAlready, lecture[i].id, (err, check) => {
                                                            if (err) {
                                                                res.send(err)
                                                            }
                                                            if (dataIsEmpty(check)) {
                                                                for (let j = 0; j < student.length; j++) {
                                                                    let setUpScore = "INSERT INTO `course_check`(`course_group`, `student_id`,`status`) VALUES (?,?,?)"
                                                                    mysql.query(setUpScore, [lecture[i].id, student[j].student_code, status], (err, result) => {
                                                                        if (err) {
                                                                            res.send(err)
                                                                        }
                                                                    })
                                                                }
                                                                let setUpdate = "INSERT INTO `check_date`(`course_group`) VALUES (?)"
                                                                mysql.query(setUpdate, lecture[i].id, (err, result) => {
                                                                    if (err) {
                                                                        res.send(err)
                                                                    }
                                                                })
                                                            }
                                                            else {
                                                                let dateCheck = new Date().toLocaleDateString()
                                                                let timeCheck = new Date(check[0].time_check).toLocaleDateString()
                                                                if (dateCheck === timeCheck) {

                                                                }
                                                                else {
                                                                    for (let j = 0; j < student.length; j++) {
                                                                        let setUpScore = "INSERT INTO `course_check`(`course_group`, `student_id`,`status`) VALUES (?,?,?)"
                                                                        mysql.query(setUpScore, [lecture[i].id, student[j].student_code, status], (err, result) => {
                                                                            if (err) {
                                                                                res.send(err)
                                                                            }
                                                                        })
                                                                    }
                                                                    let setUpdate = "INSERT INTO `check_date`(`course_group`) VALUES (?)"
                                                                    mysql.query(setUpdate, lecture[i].id, (err, result) => {
                                                                        if (err) {
                                                                            res.send(err)
                                                                        }
                                                                    })
                                                                }
                                                            }
                                                        })
                                                    })
                                                }
                                            })
                                        }
                                    }
                                })
                            })
                        }
                    })
                    notMachine = false
                }
            }
            if (notMachine) {
                if (dataIsEmpty(ip)) {
                    let insert_machine = "INSERT INTO `machines`(`machine_name`, `status`) VALUES (?,?)"
                    mysql.query(insert_machine, [machineName, status, ''], (err, result) => {
                        if (err) throw err
                        let getMachine = "SELECT * FROM `machines` WHERE machine_name = ?"
                        mysql.query(getMachine, machineName, (err, result) => {
                            if (err) throw err
                            for (let i = 0; i < result.length; i++) {
                                if (machineName === result[i].machine_name) {
                                    if (err) {
                                        res.send(err)
                                    }
                                    let machine = result[i]
                                    let getToken = "SELECT token FROM `teacher` JOIN customer_account ON teacher.teacher_id = customer_account.teacher_id WHERE customer_account.teacher_id = ?"
                                    mysql.query(getToken, machine.teacher_id, (err, result) => {
                                        if (dataIsEmpty(result)) {
                                            let getToken = "SELECT token FROM `customer_account` WHERE role = 'S'"
                                            mysql.query(getToken, (err, result) => {
                                                res.send({ machineName: machine.machine_name, status: machine.status, token: result[0].token, role: 'S' })
                                            })
                                        }
                                        else {
                                            res.send({ machineName: machine.machine_name, status: machine.status, token: result[0].token, role: 'T' })
                                        }
                                    })
                                    notMachine = false
                                }
                            }
                        })
                    })
                }
                else {
                    let insert_machine = "INSERT INTO `machines`(`machine_name`, `status`) VALUES (?,?)"
                    mysql.query(insert_machine, [machineName, status, ip], (err, result) => {
                        if (err) throw err
                        let getMachine = "SELECT * FROM `machines` WHERE machine_name = ?"
                        mysql.query(getMachine, machineName, (err, result) => {
                            if (err) throw err
                            for (let i = 0; i < result.length; i++) {
                                if (machineName === result[i].machine_name) {
                                    if (err) {
                                        res.send(err)
                                    }
                                    let machine = result[i]
                                    let getToken = "SELECT token FROM `teacher` JOIN customer_account ON teacher.teacher_id = customer_account.teacher_id WHERE customer_account.teacher_id = ?"
                                    mysql.query(getToken, machine.teacher_id, (err, result) => {
                                        if (dataIsEmpty(result)) {
                                            let getToken = "SELECT token FROM `customer_account` WHERE role = 'S'"
                                            mysql.query(getToken, (err, result) => {
                                                res.send({ machineName: machine.machine_name, status: machine.status, token: result[0].token, role: 'S' })
                                            })
                                        }
                                        else {
                                            res.send({ machineName: machine.machine_name, status: machine.status, token: result[0].token, role: 'T' })
                                        }
                                    })
                                    notMachine = false
                                }
                            }
                        })
                    })
                }
            }
        })
    }
    else {
        res.status(400).send({ statusCode: 1, message: "your apikey incorrect" })
    }
})

machine.get('/fingerprint/student/:id', (req, res) => {
    let courseGroupId = req.params.id
    let token = req.header('token')
    let getToken = "SELECT * FROM `customer_account` WHERE role = 'T'"
    let students = []
    mysql.query(getToken, (err, result) => {
        if (err) {
            res.send(err)
        }
        let teacher = result[0]
        for (let i = 0; i < result.length; i++) {
            if (result[i].token === token) {
                mysql.query(getToken, (err, result) => {
                    if (err) {
                        res.send(err)
                    }
                    for (let i = 0; i < result.length; i++) {
                        if (result[i].token === token) {
                            let getStudent = "SELECT fingerprint.student_id,fingerprint.fingerprint,fingerprint.verify FROM `student` INNER JOIN course_detail ON student.student_code=course_detail.student_code INNER JOIN fingerprint ON fingerprint.student_id=student.student_code WHERE course_detail.course_group = ?"
                            mysql.query(getStudent, courseGroupId, (err, student) => {
                                if (err) {
                                    res.send(err)
                                }
                                res.send(student)
                            })
                        }
                    }
                })
                // let dayOfWeek = "SELECT DAYOFWEEK(CURRENT_TIMESTAMP)-1 as day"
                // mysql.query(dayOfWeek, (err, result) => {
                //     if (err) {
                //         res.send(err)
                //     }
                //     let day = result[0].day
                //     if (day === 0) {
                //         day = 7
                //     }
                //     let lab = "SELECT * FROM `course_group` INNER JOIN courses ON course_group.course_id = courses.id INNER JOIN lab ON lab.course_group=course_group.id WHERE `teacher_id`= ?"
                //     mysql.query(lab, teacher.teacher_id, (err, rlec) => {
                //         let courseGroupId = []
                //         if (err) throw err
                //         for (let i = 0; i < rlec.length; i++) {
                //             if (rlec[i].day === day) {
                //                 let curtime = "SELECT CURRENT_TIMESTAMP()+1 as time"
                //                 mysql.query(curtime, (err, time) => {
                //                     let timeCur = time[0].time + ''
                //                     let timeOpen = parseInt(rlec[i].timeOpen)
                //                     let timeClose = parseInt(rlec[i].timeClosed)
                //                     let diffTime = timeClose - timeOpen
                //                     let t = compareTime(timeCur, convertTime(rlec[i].timeOpen), diffTime)
                //                     if (t) {
                //                         courseGroupId.push(rlec[i].course_group)
                //                     }
                //                     if (dataIsEmpty(courseGroupId)) {
                //                         let lecture = "SELECT * FROM `course_group` INNER JOIN courses ON course_group.course_id = courses.id INNER JOIN lecture ON lecture.course_group=course_group.id WHERE `teacher_id`= ?"
                //                         mysql.query(lecture, teacher.teacher_id, (err, rlec) => {
                //                             let courseGroupId = []
                //                             if (err) throw err
                //                             for (let i = 0; i < rlec.length; i++) {
                //                                 if (rlec[i].day === day) {
                //                                     let curtime = "SELECT CURRENT_TIMESTAMP()+1 as time"
                //                                     mysql.query(curtime, (err, time) => {
                //                                         let timeCur = time[0].time + ''
                //                                         let timeOpen = parseInt(rlec[i].timeOpen)
                //                                         let timeClose = parseInt(rlec[i].timeClosed)
                //                                         let diffTime = timeClose - timeOpen
                //                                         let t = compareTime(timeCur, convertTime(rlec[i].timeOpen), diffTime)
                //                                         if (t) {
                //                                             courseGroupId.push(rlec[i].course_group)
                //                                         }
                //                                         for (let i = 0; i < courseGroupId.length; i++) {
                //                                             let getStudent = "SELECT fingerprint.student_id,fingerprint.fingerprint,fingerprint.verify FROM `student` INNER JOIN course_detail ON student.student_code=course_detail.student_code INNER JOIN fingerprint ON fingerprint.student_id=student.student_code WHERE course_detail.course_group = ?"
                //                                             mysql.query(getStudent, courseGroupId[i], (err, stu) => {
                //                                                 if (err) {
                //                                                     res.send(err)
                //                                                 }
                //                                                 else {
                //                                                     students.push(stu)
                //                                                 }
                //                                                 if (i === courseGroupId.length - 1) {
                //                                                     if (!dataIsEmpty(students)) {
                //                                                         res.send(stu)
                //                                                     }
                //                                                     else {
                //                                                         res.send(students)
                //                                                     }
                //                                                 }
                //                                             })
                //                                         }
                //                                     })
                //                                 }
                //                             }
                //                         })
                //                     }
                //                     else {
                //                         for (let i = 0; i < courseGroupId.length; i++) {
                //                             let getStudent = "SELECT fingerprint.student_id,fingerprint.fingerprint,fingerprint.verify FROM `student` INNER JOIN course_detail ON student.student_code=course_detail.student_code INNER JOIN fingerprint ON fingerprint.student_id=student.student_code WHERE course_detail.course_group = ?"
                //                             mysql.query(getStudent, courseGroupId[i], (err, stu) => {
                //                                 if (err) {
                //                                     res.send(err)
                //                                 }
                //                                 if (dataIsEmpty(stu)) {
                //                                     res.send({ statusCode: -1, message: 'fingerprint not found' })
                //                                 }
                //                                 else {
                //                                     students.push(stu)
                //                                 }
                //                                 if (i === courseGroupId.length - 1) {
                //                                     if (!dataIsEmpty(students)) {
                //                                         res.send(stu)
                //                                     }
                //                                     else {
                //                                         res.send(students)
                //                                     }
                //                                 }
                //                             })
                //                         }
                //                     }

                //                 })
                //             }
                //         }
                //     })
                // })
            }
        }
    })
})

machine.get('/course/group', (req, res) => {
    let token = req.header('token')
    let getToken = "SELECT * FROM `customer_account` WHERE role = 'T'"

    mysql.query(getToken, (err, result) => {
        if (err) {
            res.send(err)
        }
        let teacherId
        let courses = []
        let s
        for (let i = 0; i < result.length; i++) {
            if (result[i].token === token) {
                teacherId = result[i].teacher_id
            }
        }
        let day
        let dayOfWeek = "SELECT DAYOFWEEK(CURRENT_DATE)-1 as day"
        mysql.query(dayOfWeek, (err, result) => {
            if (err) {
                res.send(err)
            }
            day = result[0].day
            if (day === 0) {
                day = 7
            }
            let curtime = "SELECT CURRENT_TIMESTAMP()+1 as time"
            mysql.query(curtime, (err, time) => {
                let tc = "20191120135560"
                let timeCur = time[0].time + ''
                let lecture = "SELECT * FROM `course_group` INNER JOIN courses ON course_group.course_id = courses.id INNER JOIN lecture ON lecture.course_group=course_group.id WHERE `teacher_id`= ?"
                mysql.query(lecture, teacherId, (err, lecture) => {
                    let courseGroupIds = []
                    if (err) throw err
                    for (let j = 0; j < lecture.length; j++) {
                        if (lecture[j].day === day) {
                            let timeOpen = parseInt(lecture[j].timeOpen)
                            let timeClose = parseInt(lecture[j].timeClosed)
                            let diffTime = timeClose - timeOpen
                            let t = compareTime(timeCur, convertTime(lecture[j].timeOpen), diffTime)
                            if (t) {
                                let courseGroupId = lecture[j].course_group + ''
                                let courseCode = lecture[j].course_code
                                let courseId = lecture[j].course_id
                                let courseName = lecture[j].course_name
                                let studyFormat = {
                                    format: "lecture",
                                    day: lecture[j].day,
                                    timeOpen: convertTime(lecture[j].timeOpen),
                                    timeClose: convertTime(lecture[j].timeClosed),
                                    room: lecture[j].room
                                }
                                let group = lecture[j].group_name
                                s = new Subject(courseGroupId, courseId, courseCode, courseName, studyFormat, lecture[j].group_name)
                                courseGroupIds.push(lecture[j].course_group)
                                courses.push(s)
                            }
                        }
                    }
                    let lab = "SELECT * FROM `course_group` INNER JOIN courses ON course_group.course_id = courses.id INNER JOIN lab ON lab.course_group=course_group.id WHERE `teacher_id`= ?"
                    mysql.query(lab, teacherId, (err, lab) => {
                        let courseGroupIds = []
                        if (err) throw err
                        for (let j = 0; j < lab.length; j++) {
                            if (lab[j].day === day) {
                                let timeOpen = parseInt(lab[j].timeOpen)
                                let timeClose = parseInt(lab[j].timeClosed)
                                let diffTime = timeClose - timeOpen
                                let t = compareTime(timeCur, convertTime(lab[j].timeOpen), diffTime)
                                if (t) {
                                    let courseGroupId = lab[j].course_group
                                    let courseCode = lab[j].course_code
                                    let courseId = lab[j].course_id
                                    let courseName = lab[j].course_name
                                    let studyFormat = {
                                        format: "lab",
                                        day: lab[j].day,
                                        timeOpen: convertTime(lab[j].timeOpen),
                                        timeClose: convertTime(lab[j].timeClosed),
                                        room: lab[j].room
                                    }
                                    let group = lab[j].group_name
                                    s = new Subject(courseGroupId, courseId, courseCode, courseName, studyFormat, lab[j].group_name)
                                    courseGroupIds.push(lab[j].course_group)
                                    courses.push(s)
                                }
                            }
                        }
                        if (!dataIsEmpty(courses)) {
                            res.send(courses)
                        }
                    })
                    if (dataIsEmpty(courses)) {
                        let lab = "SELECT * FROM `course_group` INNER JOIN courses ON course_group.course_id = courses.id INNER JOIN lab ON lab.course_group=course_group.id WHERE `teacher_id`= ?"
                        mysql.query(lab, teacherId, (err, lab) => {
                            let courseGroupIds = []
                            if (err) throw err
                            for (let j = 0; j < lab.length; j++) {
                                if (lab[j].day === day) {
                                    let timeOpen = parseInt(lab[j].timeOpen)
                                    let timeClose = parseInt(lab[j].timeClosed)
                                    let diffTime = timeClose - timeOpen
                                    let t = compareTime(timeCur, convertTime(lab[j].timeOpen), diffTime)
                                    if (t) {
                                        let courseGroupId = lab[j].course_group
                                        let courseCode = lab[j].course_code
                                        let courseId = lab[j].course_id
                                        let courseName = lab[j].course_name
                                        let studyFormat = {
                                            format: "lab",
                                            day: lab[j].day,
                                            timeOpen: convertTime(lab[j].timeOpen),
                                            timeClose: convertTime(lab[j].timeClosed),
                                            room: lab[j].room
                                        }
                                        let group = lab[j].group_name
                                        s = new Subject(courseGroupId, courseId, courseCode, courseName, studyFormat, lab[j].group_name)
                                        courseGroupIds.push(lab[j].course_group)
                                        courses.push(s)
                                    }
                                }
                            }
                            //res.send(courses)
                        })
                    }
                })
            })
        })
    })
})

machine.get('/list/student/:id', (req, res) => {
    let courseGroupId = req.params.id
    let token = req.header('token')
    let getToken = "SELECT * FROM `customer_account` WHERE role = 'T'"
    let students = []
    let curtime = "SELECT CURRENT_TIMESTAMP() as time"
    mysql.query(getToken, (err, result) => {
        if (err) {
            res.send(err)
        }
        for (let i = 0; i < result.length; i++) {
            if (result[i].token === token) {
                let getStudent = "SELECT student.student_code as studentCode,CONCAT(name_th.firstName,' ',name_th.lastName) AS studentName FROM `student` JOIN course_detail ON course_detail.student_code = student.student_code JOIN name_th ON name_th.nameth_id=student.name_th WHERE course_detail.course_group = ?"
                mysql.query(getStudent, courseGroupId, (err, student) => {
                    if (err) {
                        res.send(err)
                    }
                    res.send(student)
                })
            }
        }
    })
})

machine.post('/verify', (req, res) => {
    let token = req.header('token')
    let getToken = "SELECT * FROM `customer_account` WHERE role = 'T'"

    let verify = req.body.verify
    mysql.query(getToken, (err, result) => {
        if (err) {
            res.send(err)
        }
        let teacherId
        let courses = []
        for (let i = 0; i < result.length; i++) {
            if (result[i].token === token) {
                teacherId = result[i].teacher_id
            }
        }
        let day
        let dayOfWeek = "SELECT DAYOFWEEK(CURRENT_TIMESTAMP)-1 as day"
        mysql.query(dayOfWeek, (err, result) => {
            if (err) {
                res.send(err)
            }
            day = result[0].day
            if (day === 0) {
                day = 7
            }
            let timeCur
            let curtime = "SELECT CURRENT_TIMESTAMP()+1 as time"
            mysql.query(curtime, (err, time) => {
                timeCur = time[0].time + ''
                let courseId = []
                let course_id = []
                let subject = []
                let lab = "SELECT course_group.id AS courseGroupId,courses.course_id AS courseId,course_group.group_name,lab.day,lab.timeOpen,courses.course_name,lab.timeClosed FROM `course_group` INNER JOIN courses ON course_group.course_id = courses.id INNER JOIN lab ON lab.course_group=course_group.id WHERE `teacher_id`= ?"
                mysql.query(lab, teacherId, (err, lab) => {
                    if (err) {
                        res.send(err)
                    }
                    for (let i = 0; i < lab.length; i++) {
                        if (lab[i].day === day) {
                            let timeOpen = parseInt(lab[i].timeOpen)
                            let timeClose = parseInt(lab[i].timeClosed)
                            let diffTime = timeClose - timeOpen
                            let t = compareTime(timeCur, convertTime(lab[i].timeOpen), diffTime)
                            if (t) {
                                courseId.push(lab[i].courseGroupId)
                                course_id.push(lab[i].courseId)
                                subject.push(lab[i].course_name)
                            }
                        }
                    }
                    if (!dataIsEmpty(courseId)) {
                        for (let i = 0; i < courseId.length; i++) {
                            courseGroupId = courseId[i]
                            let getDate = "SELECT * FROM `course_check` WHERE course_group = ?"
                            let curTime = new Date().toLocaleDateString()
                            mysql.query(getDate, courseGroupId, (err, date) => {
                                let courseGroupCheck
                                if (err) {
                                    res.send(err)
                                }
                                if (dataIsEmpty(date)) {
                                }
                                else {
                                    for (let j = 0; j < date.length; j++) {
                                        let timeCheck = new Date(date[j].time_check).toLocaleDateString()
                                        if (timeCheck === curTime) {
                                            courseGroupCheck = date[j].course_group
                                        }
                                    }
                                }
                                let stuId
                                let getFingerVerify = "SELECT * FROM `course_check`JOIN fingerprint ON fingerprint.student_id=course_check.student_id WHERE course_group = ?"
                                mysql.query(getFingerVerify, courseGroupCheck, (err, fingerprint) => {
                                    if (err) {
                                        res.send(err)
                                    }
                                    if (dataIsEmpty(fingerprint)) {
                                    }
                                    else {
                                        for (let j = 0; j < fingerprint.length; j++) {
                                            let timeCheck = new Date(fingerprint[j].time_check).toLocaleDateString()
                                            if (verify === fingerprint[j].verify && timeCheck === curTime) {
                                                stuId = fingerprint[j].student_id
                                            }
                                        }
                                        if (!dataIsEmpty(stuId)) {
                                            let updateScore = "UPDATE `course_check` SET `score` = '1' WHERE student_id = ?"
                                            mysql.query(updateScore, stuId, (err, result) => {
                                                if (err) {
                                                    res.send(err)
                                                }
                                                res.send({ courseId: course_id[i], courseGroupId: courseId[i], subject: subject[i], student: stuIdd })
                                            })
                                        }
                                    }
                                })
                            })
                        }
                    }
                    else {
                        let lecture = "SELECT course_group.id AS courseGroupId,courses.course_id AS courseId,course_group.group_name,lecture.day,lecture.timeOpen,courses.course_name,lecture.timeClosed FROM `course_group` INNER JOIN courses ON course_group.course_id = courses.id INNER JOIN lecture ON lecture.course_group=course_group.id WHERE `teacher_id`= ?"
                        mysql.query(lecture, teacherId, (err, lecture) => {
                            if (err) {
                                res.send(err)
                            }
                            for (let i = 0; i < lecture.length; i++) {
                                if (lecture[i].day === day) {
                                    let timeOpen = parseInt(lecture[i].timeOpen)
                                    let timeClose = parseInt(lecture[i].timeClosed)
                                    let diffTime = timeClose - timeOpen
                                    let t = compareTime(timeCur, convertTime(lecture[i].timeOpen), diffTime)
                                    if (t) {
                                        courseId.push(lecture[i].courseGroupId)
                                        course_id.push(lecture[i].courseId)
                                        subject.push(lecture[i].course_name)
                                    }
                                }
                            }
                            if (!dataIsEmpty(courseId)) {
                                for (let i = 0; i < courseId.length; i++) {
                                    courseGroupId = courseId[i]
                                    let getDate = "SELECT * FROM `course_check` WHERE course_group = ?"
                                    let curTime = new Date().toLocaleDateString()
                                    mysql.query(getDate, courseGroupId, (err, date) => {
                                        let courseGroupCheck
                                        if (err) {
                                            res.send(err)
                                        }
                                        if (dataIsEmpty(date)) {
                                        }
                                        else {
                                            for (let j = 0; j < date.length; j++) {
                                                let timeCheck = new Date(date[j].time_check).toLocaleDateString()
                                                if (timeCheck === curTime) {
                                                    courseGroupCheck = date[j].course_group
                                                }
                                            }
                                        }
                                        if (!dataIsEmpty(courseGroupCheck)) {
                                            let stuIdd
                                            let getFingerVerify = "SELECT * FROM `course_check`JOIN fingerprint ON fingerprint.student_id=course_check.student_id WHERE course_group = ?"
                                            mysql.query(getFingerVerify, courseGroupCheck, (err, fingerprint) => {
                                                if (err) {
                                                    res.send(err)
                                                }
                                                if (dataIsEmpty(fingerprint)) {
                                                    let obj = {}
                                                    res.send(obj)
                                                }
                                                else {
                                                    for (let j = 0; j < fingerprint.length; j++) {
                                                        let timeCheck = new Date(fingerprint[j].time_check).toLocaleDateString()
                                                        if (verify === fingerprint[j].verify && timeCheck === curTime) {
                                                            stuIdd = fingerprint[j].student_id
                                                        }
                                                    }
                                                    if (!dataIsEmpty(stuIdd)) {
                                                        let updateScore = "UPDATE `course_check` SET `score` = '1' WHERE student_id = ?"
                                                        mysql.query(updateScore, stuIdd, (err, result) => {
                                                            if (err) {
                                                                res.send(err)
                                                            }
                                                            res.send({ courseId: course_id[i], courseGroupId: courseId[i], subject: subject[i], student: stuIdd })
                                                        })
                                                    }
                                                }
                                            })
                                        }
                                    })
                                }
                            }
                        })
                    }
                })
            })
        })
    })
})

module.exports = machine